import React, {Component} from 'react';
import { script_tab} from '../Object/Sweet.js';

export default class Script extends Component
{
	render()
	{
		return(
			<div>
			{script_tab.map(({id, links}) => (
				<script key={id} src = {links}</script>
			)
			)
			}
			</div>
		);
	}
}
