import React, { Component } from 'react';
import { numbers } from './Component/Object/Sweet.js'
import Splash  from './Component/View/Splash.js'
import Button from './Component/View/Button.js'
import Link from './Component/View/Link.js'
import Famille from './Component/View/Famille.js'

class App extends Component {
  render() {
    return(
		<div>
		<ul>
		{ numbers.map((number) => ( <li>{number}</li>))}
		</ul>
	    	<Splash />
	    	<Button /><br />
	    	<Link /><br />
	    	<Famille />
		</div>
	);
  }
}
export default App;
