import React, {Component} from 'react';

export default class Header extends Component
{
	render()
	{
		return(
			<header id="header">
				<div className="logo">
					<span className="icon fa-diamond"></span>
				</div>
				<div className="content">
					<div className="inner">
						<h1>A dynamiser</h1>
						<p>A dynamiser</p>
					</div>
				</div>
				<nav>
					<ul>
						<li><a href="#intro">intro</a></li>
					</ul>
				</nav>
			</header>
		);
	}
}
